
const test = require('ava'),
      nats = require('nats'),
      subTime = 15;


var ncDispatcher,
    ncWorker;

test.before(()=>{
  ncDispatcher = nats.connect({servers: ['nats://dispatcher:bar@nats:4222']});
  ncWorker = nats.connect({servers: ['nats://worker:foo@nats:4222']});
});

test.cb('pub to workers.queue topic', (t)=>{
  t.plan(1);
  let message = 'Test message!',
      topic = 'workers.queue';

  let sub = ncWorker.subscribe(topic, (msg)=>{
    t.is(msg, message, 'Message did not match.');
    ncWorker.unsubscribe(sub);
    clearTimeout(timer);
    t.end();
  });

  // Allow sub to connect
  setTimeout(()=>{
    ncDispatcher.publish(topic, message);
  }, subTime);

  // Dont allow test to hang
  let timer = setTimeout(()=>{
    ncWorker.unsubscribe(sub);
    t.fail('Test timeout occured.');
    t.end();
  }, 2000);
});

test.cb('pub to workers.status topic', (t)=>{
  t.plan(1);
  let message = 'Test message!',
      topic = 'workers.status';

  let sub = ncDispatcher.subscribe(topic, (msg)=>{
    t.is(msg, message, 'Message did not match.');
    ncDispatcher.unsubscribe(sub);
    clearTimeout(timer);
    t.end();
  });

  setTimeout(()=>{
    ncWorker.publish(topic, message);
  }, subTime);

  let timer = setTimeout(()=>{
    ncDispatcher.unsubscribe(sub);
    t.fail('Test timeout occured.');
    t.end();
  }, 2000);
});


test.cb('worker should not be able to pub to workers.queue', (t)=>{
  t.plan(1);
  let message = 'Test message!',
      topic = 'workers.queue';

  let sub = ncWorker.subscribe(topic, (msg)=>{
    t.not(msg, message, 'Message did match, but should not even have a message.');
    t.end();
  });

  setTimeout(()=>{
    ncWorker.publish(topic, message);
  }, subTime);

  // Wait for message to not come in
  setTimeout(()=>{
    ncWorker.unsubscribe(sub);
    t.pass();
    t.end();
  }, 500);
});

test.cb('dispatcher should not be able to pub to workers.status', (t)=>{
  t.plan(1);
  let message = 'Test message!',
      topic = 'workers.status';

  let sub = ncDispatcher.subscribe(topic, (msg)=>{
    t.not(msg, message, 'Message did match, but should not even have a message.');
    t.end();
  });

  setTimeout(()=>{
    ncDispatcher.publish(topic, message);
  }, subTime);

  // Wait for message to not come in
  setTimeout(()=>{
    ncDispatcher.unsubscribe(sub);
    t.pass();
    t.end();
  }, 500);
});